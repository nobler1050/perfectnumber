# Exercise #

>Write a program that uses functions to determine if a number is a perfect
>number. Your program should have a function called is_perfect, that takes
>in a single parameter. If the parameter sent in is a perfect number, then
>it returns True. Otherwise it returns False.  def is_perfect(num):
