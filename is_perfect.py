#!/bin/python
""" homework assignment 2 """

import sys

def is_perfect(number):
    """ input number check if perfect and  return boolean  """
    number = int(number)
    if number == 1:
        return False # Fixes bug when testing
    sumfactors = 1 # Adding 1 since its being skipped
    for divisor in xrange(2, number): # Skipping 1 since it always works
        if number % divisor == 0:
            quotient = number / divisor
            if divisor >= quotient:
                break # Break when reached all divisors
            sumfactors += quotient + divisor
            if sumfactors > number:
                break # Break if not perfect
    if sumfactors == number:
        return True
    else:
        return False

def getdivisors(number): # This function is not used any longer
    """ input number return a list of divisors """
    number = int(number)
    lower = []
    higher = []
    divisor = 1
    while True:
        if number % divisor == 0:
            quotient = number / divisor
            if divisor >= quotient:
                break #break when reached all divisors
            lower.append(divisor)
            higher.append(quotient)
        divisor += 1
    divisors = lower + higher[::-1]
    return divisors

try:
    print is_perfect(sys.argv[1])
except (ValueError, IndexError):
    print "Usage: %s [INTEGER]..." %sys.argv[0]
    print "Returns True for perfect numbers else False.\n"
    print "Examples:"
    print "  %s 6\tReturns True" % sys.argv[0]
    print "  %s 8\tReturns False"% sys.argv[0]


#divisors = getDivisors(sys.argv[1])
#print divisors
